# libraries ----------
library(shiny)
library(data.table)
library(covid19india)
library(tidyverse)
library(plotly)
library(gridExtra)
library(ggtext)
library(grid)
library(extrafont)
library(gt)
library(glue)

# source scripts and functions ----------
f <- list.files("plot_functions")
for (i in seq_along(f)) {source(paste0("plot_functions/", f[i]))}

source("top_matter.R", local = TRUE)
source("references.R", local = TRUE)
source("observed.R", local = TRUE)
source("state.R", local = TRUE)
source("metrics.R", local = TRUE)

# load data -----------
state_count_data <- get_state_counts()
all_the_data     <- get_all_data()[, .(place, abbrev, date, r = r_est, lower = r_lower, upper = r_upper)]
nat_count_data   <- get_nat_counts()
vax_data         <- get_state_vax()


# extract names and abbrevs ----------
state_abbrev <- covid19india::pop[, .SD[1], by = place][order(place), abbrev]
state_names  <- covid19india::pop[, .SD[1], by = place][order(place), place]

# pre-generate state plots ----------
plots <- list()
for (i in seq_along(state_abbrev)) {
  plots[[state_abbrev[i]]][[paste0("daily_barplot")]] <- state_daily_barplot(data = state_count_data, forecast = state_abbrev[i])
  plots[[state_abbrev[i]]][[paste0("r_plot")]] <- tvr_plot(data = all_the_data, forecast = state_abbrev[i])
}

# Define server logic required to draw a histogram
shinyServer(function(input, output) {

  # extract and render function -----------
  extract_and_render_plot <- function(plot, plot_name, state_code) {
    print(class(plot))
    if(is.null(plot) | inherits(plot, "simpleError")) {
      out <- NULL
    } else if ("plotly" %in% class(plot))
      out <- tryCatch(expr = { renderPlotly(plot) },
                      error = function(e) {
                        showNotification('there was an error','',
                                         type = "error"); return() }, silent = TRUE)
    else if ("ggplot" %in% class(plot))
      out <- tryCatch(expr = { renderPlot(plot) },
                      error = function(e) {
                        showNotification('there was an error','',
                                         type = "error"); return() }, silent = TRUE)
    else if ("grob" %in% class(plot))
      out <- renderPlot(grid.arrange(plot))
    else {
      print(class(plot))
      stop("Unrecognized plot type!")
    }
    output[[paste(state_code, plot_name, sep = "_")]] <<- out
  }

  # render plot outputs ------------
  output$india_daily_barplot     <- renderPlotly( india_daily_barplot() )
  output$india_daily_vax         <- renderPlotly( india_daily_vax() )
  output$case_death_country_comp <- renderPlotly( case_death_country_comp() )
  output$case_facetplot          <- renderPlot( top_case_death_facetplot()$case_facet )
  output$death_facetplot         <- renderPlot( top_case_death_facetplot()$death_facet )
  output$india_r_plot            <- renderPlotly( tvr_plot(data = all_the_data, forecast = "India") )
  output$r_forest                <- renderPlot( r_forest_plot(x = all_the_data) )

  # extract and render state forecasts ------------
  walk2(plots, state_abbrev, function(states_plots, state_code) {
      iwalk(states_plots, extract_and_render_plot, state_code = state_code)
    }
  )

  # render pages -----------
  output$out <- renderUI({

    tabs <- purrr::map2(state_names, state_abbrev, generate_state_tab)

    eval(expr(navbarPage("COVID-19 Outbreak in India",
                         observed, navbarMenu("States", !!!tabs),
                         metrics, references)))

  })

  # render metrics table ----------
  output$metrics_table = render_gt({
    metrics_table
  })

  # render snapshot table on landing page -------------
  output$gt_india_snapshot = render_gt({ snapshot() })

})
